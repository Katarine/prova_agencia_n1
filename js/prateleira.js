/*
    Author: Katarine Albuquerque
*/

/*
    Documento feito pelos dados do arquivo JSON 
    Feito direto pelo Javascript.
    Valores impressos pelo console.log
*/

$(document).ready(function(){

    

    var valores = [    
       {
            "name": "Action figure Goku",
            "price": 200.90,
            "image": "./src/img/thumb_goku.jpg"
        },
        {
            "name": "Action figure Samus",
            "price": 120.80,
            "image": "./src/img/thumb_samus.jpg"
        },
        {
            "name": "Action figure Sub-zero",
            "price": 200.90,
            "image": "./src/img/thumb_subzero.jpg"
        },
        {
            "name": "Action figure Link",
            "price": 700.90,
            "image": "./src/img/thumb_link.jpg"
        }
    ];

    console.log("-------------------------");
    console.log("-----  | Products | -----");
    console.log("-------------------------");

    for(var i = 0 ; i < 4 ; i++) { 

        console.log("Name: " + valores[i].name);
        console.log("Price: " + valores[i].price);
        console.log("Image: " + valores[i].image);
        console.log("----- // -----");        
    }
    
});


