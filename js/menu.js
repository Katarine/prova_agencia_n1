/*
    Author: Katarine Albuquerque
*/

$(document).ready(function(){

	$('#navegacao').css("background-color","#000");
	$("#img1").show();
	$("#img2").hide();

	$(".click").click(function(){
		$("#org_menu").show(300);
		$("#img1").hide();
		$("#img2").show();
		$('#navegacao').css("background-color","#084154");
	});

	$(".close").click(function(){
		$("#org_menu").hide(300);
		$("#img1").show();
		$("#img2").hide();
		$('#navegacao').css("background-color","#000");
	});
});