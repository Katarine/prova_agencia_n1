/*
    Author: Katarine Albuquerque
*/

$(document).ready(function(){

	$(".abrir").click(function(){
		$(".janela").show(400);		
	});

	$("#close").click(function(){
		$(".janela").hide(400);
	});

	var valores = [1,2,3,4];
	var v = valores.length;

	$("#p").prepend(v.toString());	
	
});
